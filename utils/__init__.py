import numpy as np
from skimage.feature import hog


def rgb2gray(rgb):
    return np.dot(rgb[..., :3], [0.299, 0.587, 0.144])


def extract_hog(img, orientations, pixels_per_cell, cells_per_block):
    gray = rgb2gray(img)

    return hog(
        image=gray,
        orientations=orientations,
        pixels_per_cell=pixels_per_cell,
        cells_per_block=cells_per_block,
        block_norm='L2-Hys',
        visualize=False,
        transform_sqrt=True
)